# Custom commands

Some commands to put under my PATH enviroment variable to make my life easier

```
sudo chmod +x /home/custom_commands/*
```
``` 
nano ~/.profile
```
```
export PATH=/home/custom_commands:$PATH
```

- **gitll**: pulls from origin master
- **gitcmall**: adds and commit everything with a comment
- **gitsh**: push to origin local branch
- **gitall**: pulls from origin master & push to origin local branch
- **gitallbranches**: fetch locally all remote branches
- **dbcreate** <NAME>: create a <NAME> database in postgres with user: <NAME> pass: <NAME>  
- **dbusrchange** <DB_NAME> <USR_NAME>: Changes the ownership of all the public tables of the postgres db
- **dbbk** <NAME>: Creates a back up (psql dump) of you <NAME> database
- **dboverwrite** <FILE_NAMe> <DB_NAME>: Destroys the db <DB_NAME> (it does a bk too) and overwrites it with the <FILE_NAME>
- **dbowner** <NAME>: Sets the owner of all the tables of the database <NAME> to the user <NAME> 
- **morevol**: increase +20% the volume of the machine (be careful: it has no limits)
- **withservicedestroy** <NAME>: removes the source code and virtualenv from /var/apps, destroys all the service definitions under /etc/systemd/system/ and soft reload systemctl 
- **poetry_env**: It activates the current directory poetry virtual environment, you might wonder why just not `poetry shell` well I find that command quite buggy